package com.robert.vesta.operator;

import org.testng.annotations.*;

/**
 * @author xiejinjun
 */
public class OperationTest {
    //1023
    @org.testng.annotations.Test
    public void testName() throws Exception {
        System.out.println(-1L ^ -1L); // 0

        System.out.println(-1L << 10); //-1024

        System.out.println(0 << 10); //0

        System.out.println(-1L ^ -1L << 10); //1023

        System.out.println(-1L ^ -1024); //1023
    }
}
