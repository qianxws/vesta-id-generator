package com.robert.vesta.service.impl.converter;

import com.robert.vesta.service.bean.Id;

/**
 * ID转换器
 * <p>ID对象和long之间的转换</p>
 */
public interface IdConverter {

	long convert(Id id);

	Id convert(long id);

}
